class AvatarMax{
  //The avatar class, creating an object named avatar.
  //The avatar can move in x- and y-direction in a specified area on the screen according to x- and y-speed.
  float avx, avy, avscale, xspeed, yspeed, xleftbound, xrightbound, yupperbound, ylowerbound, yArms = 0, yAoffset = 1;
  boolean bx = false, by = false;
  IntList bbMax;
  

  AvatarMax(float x, float y, float scale){
    //constructor; setting the avatar's x- and y-location and its scale.
    avx = x; 
    avy = y;
    avscale = scale;
    bbMax = new IntList();
  }
  
  void display(){
   //draws avatar according to values of avx, avy, avscale
   //legs+arms
   stroke(0);
   strokeWeight(3*avscale);
   line(avx-20*avscale, avy+90*avscale, avx-25*avscale, avy+120*avscale);
   line(avx+20*avscale, avy+90*avscale, avx+25*avscale, avy+120*avscale);
   line(avx-50*avscale, avy, avx-70*avscale, avy+yArms*avscale);
   line(avx+50*avscale, avy, avx+70*avscale, avy+yArms*avscale);
  
   //body
   noStroke();
   ellipseMode(CENTER);
   fill(104,131,140);
   ellipse(avx, avy, 100*avscale, 200*avscale);
   
   //eye
   stroke(0);
   strokeWeight(1);
   fill(255);
   ellipse(avx, avy-30*avscale, 50*avscale, 100*avscale);
   fill(255,255,0);
   ellipse(avx-5*avscale, avy-20*avscale, 30*avscale, 30*avscale);
   fill(0);
   ellipse(avx-5*avscale, avy-20*avscale, 15*avscale, 15*avscale);
   //avatar winks if key is pressed
   if (keyPressed){
     if(key == ENTER){
       fill(104,131,140);
       ellipse(avx, avy-30*avscale, 50*avscale, 100*avscale);
     }
   }
   //mouth
   fill(200);
   arc(avx, avy+20*avscale, 60*avscale, 50*avscale, 0, PI, OPEN);
   fill(104,131,140);
   arc(avx, avy+20*avscale, 60*avscale, 45*avscale, 0, PI, OPEN);
   fill(0);
   triangle(avx-5*avscale, avy+50*avscale, avx+5*avscale, avy+50*avscale, avx, avy+57*avscale);
   //hat
   line(avx-40*avscale, avy-90*avscale, avx+40*avscale, avy-90*avscale);
   fill(139,69,19);
   rect(avx-25*avscale, avy-105*avscale, 50*avscale, 15*avscale);
   fill(100);
   rect(avx-25*avscale, avy-98*avscale, 50*avscale, 5*avscale);
  getBoundingBox();
}
  
  void move(float xs, float ys, float xb1, float xb2, float yb1, float yb2){
  //moves the avatar across the screen according to values for 
  //xspeed, yspeed, x- and y-bounds  
  xspeed = xs;
  yspeed = ys;
  xleftbound = xb1;
  xrightbound = xb2;
  ylowerbound = yb1;
  yupperbound = yb2;
  
  //if the avatar hits a bound, the state of bx or by is changed.
    if(avx > xrightbound && xspeed > 0){
      bx = true;
    }else if(avx > xrightbound && xspeed < 0){
      bx = false;
    }else if(avx < xleftbound && xspeed > 0){
      bx = false;
    }else if(avx < xleftbound && xspeed < 0){
      bx = true;
    }
    
    if(avy > yupperbound && yspeed > 0){
      by = true;
    }else if(avy > yupperbound && yspeed < 0){
      by = false;
    }else if(avy < ylowerbound && yspeed > 0){
      by = false;
    }else if(avy < ylowerbound && yspeed < 0){
      by = true;
    }
    
  //the x- and y-location of avatar changes according to the state of bx and by.
    if (bx == true){
      avx = avx - xspeed;
    }else if (bx == false){
      avx = avx + xspeed;
    }
  
    if (by == true){
      avy = avy - yspeed;
    }else if (by == false){
      avy = avy + yspeed;
    } 
  //lets the arms wiggle  
    if((yArms < 0) || (yArms > 20)){
      yAoffset = -yAoffset;
    }
    yArms = yArms + yAoffset;
  }
  
  IntList getBoundingBox(){
    setBoundingBox();
      //rectMode(CORNERS);
      //rect(bbMax.get(0),bbMax.get(1),bbMax.get(2),bbMax.get(3));
      //rectMode(CENTER);
    return bbMax;
  }
  
  void setBoundingBox(){
    bbMax.clear();
    
    bbMax.append(round(avx-70*avscale));
    bbMax.append(round(avy-105*avscale));
    
    bbMax.append(round(avx+70*avscale));
    bbMax.append(round(avy+120*avscale));
  }
}