class Cartman{
  int centerX,
      centerY,
      headLX,
      headLY,
      bodyX,
      bodyY,
      rEyeX,
      rEyeY,
      lEyeX,
      lEyeY,
      rPupilX,
      rPupilY,
      lPupilX,
      lPupilY,
      lLegX,
      lLegY,
      rLegX,
      rLegY,
      mouthShape1,
      mouthShape2,
      hatX,hatY,
      baseCenterY,
      changableCenterY,
      alpha = 255;
      
  boolean flag,
          jumpFlag,
          jumpFlagContinue,
          CartmanFlag;
  
  int headProportionX = 150,
      headProportionY = 110,
      eyeSizeX = 20,
      eyeSizeY = 25,
      pupilSizeX,
      pupilSizeY;
  IntList boundingBox;
  
  //Default Constructor
  Cartman(){
  
      flag = false;
      centerX = displayWidth/2;
      centerY = displayHeight/2;
      headLX = centerX;
      headLY = (centerY)-100;
      rEyeX = headLX - 20;
      rEyeY = headLY - 20;
      lEyeX = headLX + 20;
      lEyeY = headLY - 20;
      bodyX = centerX;
      bodyY = centerY;
      rLegX = bodyX+25;
      rLegY = bodyY+70;
      lLegX = bodyX-25;
      lLegY = bodyY+70;
      mouthShape1 = 25;
      mouthShape2 = 15;
      hatX = headLX-70;
      hatY = headLY-40;
      baseCenterY = centerY;
      changableCenterY = centerY;
      jumpFlag = false;
      jumpFlagContinue = false;
      boundingBox = new IntList();
      setBoundingBox();
  }
  
    //constructor with values
    Cartman(int mouthWagle, int centerXP, int centerYP){
      flag = false;
      
      //Set up my character
      centerX = centerXP;
      centerY = centerYP;
      headLX = centerX;
      headLY = (centerY)-100;
      rEyeX = headLX - 20;
      rEyeY = headLY - 20;
      lEyeX = headLX + 20;
      lEyeY = headLY - 20;
      bodyX = centerX;
      bodyY = centerY;
      rLegX = bodyX+25;
      rLegY = bodyY+70;
      lLegX = bodyX-25;
      lLegY = bodyY+70;
      mouthShape1 = 25;
      mouthShape2 = mouthWagle;
      hatX = headLX-70;
      hatY = headLY-40;
      baseCenterY = centerY;
      changableCenterY = centerY;
      jumpFlag = false;
      jumpFlagContinue = false;
      boundingBox = new IntList();
      setBoundingBox();
      
    }
    
    void display(){
      //moves mouth
      tickMouth();
      
      headLX = centerX;
      headLY = (centerY)-100;
      rEyeX = headLX - 20;
      rEyeY = headLY - 20;
      lEyeX = headLX + 20;
      lEyeY = headLY - 20;
      
      bodyX = centerX;
      bodyY = centerY;
      
      rLegX = bodyX+25;
      rLegY = bodyY+70;
      lLegX = bodyX-25;
      lLegY = bodyY+70;
      lLeg();
      rLeg();
      
      body();
      
      head();
      
      rHand();
      lHand();
      
      hat();
    
      
      
      if(jumpFlagContinue == true){
        jump();
      }
      
      //setBoundingBox();
      getBoundingBox();
     
      profanity();
      
      //moves bus
      CartmanLate();
    }
    
    
    IntList getBoundingBox(){
      setBoundingBox();
//      rectMode(CORNERS);
//      rect(boundingBox.get(0),boundingBox.get(1),boundingBox.get(2),boundingBox.get(3));
//      rectMode(CENTER);
      return boundingBox;
    }
    
    void setBoundingBox(){
      boundingBox.clear();
      
      boundingBox.append(centerX-115);//left side x bound
      boundingBox.append(centerY - 150);
      
      boundingBox.append(centerX+115);//right side x bound
      boundingBox.append(centerY + 150);
    }
    
    void head(){
      ellipseMode(CENTER);
      //Head
      fill(255,218,181,alpha);
      ellipse(headLX,headLY,headProportionX,headProportionY);
      fill(255,255,255,alpha);
      eyes();
      //Mouth
      fill(0,0,0,alpha);
      ellipse(headLX,headLY+30,mouthShape1,mouthShape2);
      fill(255,255,255,alpha);
      //arc(headLX,headLY-20, 150, 150, PI, PI+PI);
    }
  
    void eyes(){
      ellipse(rEyeX,rEyeY,eyeSizeX,eyeSizeY);
      ellipse(lEyeX,lEyeY,eyeSizeX,eyeSizeY);
      fill(0,0,0,alpha);
      ellipse(rEyeX+3,rEyeY,eyeSizeX/2,eyeSizeY/2);
      ellipse(lEyeX-3,lEyeY,eyeSizeX/2,eyeSizeY/2);
      fill(255,255,255,alpha);
    }
    void body(){
      ellipseMode(CENTER);
      fill(255,0,0,alpha);
      ellipse(bodyX,bodyY,225,150);
      fill(255,255,255,alpha);
    }
  
    void rHand(){
      ellipseMode(CENTER);
      fill(255,218,181,alpha);
      ellipse(bodyX-45,bodyY,30,20);
      fill(255,255,255,alpha);
    }
    void lHand(){
      ellipseMode(CENTER);
      fill(255,218,181,alpha);
      ellipse(bodyX+45,bodyY,30,20);
      fill(255,255,255,alpha);
    }
    void lLeg(){
       rectMode(CENTER);
       fill(0,0,0,alpha);
       rect(lLegX,lLegY,40,75);
        fill(255,255,255,alpha);
    }
    void rLeg(){
      rectMode(CENTER);
      fill(0,0,0,alpha);
      rect(rLegX,rLegY,40,75);
      fill(255,255,255,alpha);
    }
  
    void moveLeft(){
      if(centerX > displayWidth){
        flag = true;
      }
      else if(centerX < 0){
        flag = false;
      }
  
      if(flag ==false){
        centerX = centerX+3;
      }else if (flag ==true){
         centerX = centerX - 3;
      }
    }
  
    void tickMouth(){
      if(mouthShape1 < 25){
      mouthShape1++;
      //mouthShape2 = ;
      }
      else{
        mouthShape1= 10;
      }
    }
    

    void hat(){
  
      fill(0,204,204,alpha);
       beginShape();
       vertex(headLX-70,headLY-25);
       vertex(headLX-60, headLY-40);
       vertex(headLX-55, headLY-45);
       vertex(headLX-50, headLY-50);
       vertex(headLX-45, headLY-52);
       vertex(headLX-40, headLY-55);
       vertex(headLX-35, headLY-56);
       vertex(headLX-30, headLY-58);
       vertex(headLX-25, headLY-58);
       vertex(headLX-20, headLY-59);
       vertex(headLX-15, headLY-59);
       vertex(headLX-15, headLY-59);
       vertex(headLX-10, headLY-59);
       vertex(headLX-5, headLY-59);
       vertex(headLX, headLY-59);
       vertex(headLX+5, headLY-59);
       vertex(headLX+10, headLY-59);
       vertex(headLX+15, headLY-59);
       vertex(headLX+20, headLY-58);
       vertex(headLX+25, headLY-58);
       vertex(headLX+30, headLY-57);
       vertex(headLX+35, headLY-56);
       vertex(headLX+40, headLY-54);
       vertex(headLX+45, headLY-52);
       vertex(headLX+50, headLY-50);
       vertex(headLX+55, headLY-47);
       vertex(headLX+60, headLY-43);
       vertex(headLX+65, headLY-36);
       vertex(headLX+70, headLY-25);
        endShape(CLOSE);
        fill(255,255,255,alpha);
        fill(255,255,0,alpha);
        ellipse(headLX,headLY-65,30,30);
        fill(255,255,255,alpha);
    }
    
    void jump(){
      
      if(centerY > baseCenterY-200 && jumpFlag == false){
        centerY -= 2;
    //upper limit for jump
      }else if (centerY >= baseCenterY-200 && jumpFlag == true){
        centerY += 4;
      }
  
      if (centerY <= baseCenterY-200){
        jumpFlag = true;
      }
      if(centerY == baseCenterY){
        jumpFlag = false;
        jumpFlagContinue = false;
      }
  }
  
  //sets the flag for bus movement
  void CartmanLate(){
    if(centerX >=displayWidth){
      CartmanFlag = true;
    }else if(centerX <= 2){
          CartmanFlag = false;
    }
  }
  //display text when Cartman hits wall
  void profanity(){
    if (centerX > displayWidth-15 && centerX < displayWidth+3)
      text("No Mr. Kitty, thats a bad kitty!!!", displayWidth-250 ,185);
  }
  
  void debugCartman(){
    
  }
  
  void collideAnim(){
        centerX = centerX+3;
  }
}
