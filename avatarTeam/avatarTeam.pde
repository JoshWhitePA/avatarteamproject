int x = 800, y = 600;
char keyType = 'i';
float fill3 = 0, alpha = 0, foffset = 1, aloffset = 1;
 Cartman cartman;
 AvatarMax max;
 Overlap ol;
 Randal randy;
  //tree arrays
//-------------------------
int[] x1t = new int[50];
int[] x2t = new int[50];
int[] yt = new int[50];
int[] ht = new int[50];
int[] xht = new int[50];
int[] fill = new int[50];
//--------------------------
int[] x1t2 = new int[50];
int[] x2t2 = new int[50];
int[] yt2 = new int[50];
int[] ht2 = new int[50];
int[] xht2 = new int[50];
int[] fill2 = new int[50];
int[] alpha2 = new int[50];
//--------------------------

void setup(){
  size(800, 600);
  frameRate(60);
  randy = new Randal(400,300,2);
  cartman = new Cartman();
  max = new AvatarMax(0,300,1);
  ol = new Overlap();
  //fill the arrays for trees with values
  for(int i = 0; i < 50; i++){
   x1t[i] = (int) random(0,width-100);
   x2t[i] = x1t[i] + (int)random(20,100);
   yt[i] = (int)random(0,50) + (height-150);
   ht[i] = (height-150)-(int)random(40,100);
   xht[i] = ((x2t[i]-x1t[i])/2)+x1t[i];
   fill[i] = (int)random(50,200);
   println("done " + i);
  } 
   for(int k = 0; k < 50; k++){
   x1t2[k] = (int) random(0,width-100);
   x2t2[k] = x1t2[k] + (int)random(20,100);
   yt2[k] = (int)random(0,100) + (height-1);
   ht2[k] = (height-1)-(int)random(40,100);
   xht2[k] = ((x2t2[k]-x1t2[k])/2)+x1t2[k];
   fill2[k] = (int)random(50,200);
   alpha2[k] = (int)random(0,255);
   println("done2 " + k);
  }
 }
//---------------------------------------------



void drawBackground1(){
  //places sky and sun as primary background
  background(136, 206, 250);
  noStroke();
  ellipseMode(CENTER);
  fill(255, 145, 0);
  ellipse(250, 500, 500, 500);
  fill(255, 175, 0);
  ellipse(250, 500, 400, 400);
  fill(255, 205, 0);
  ellipse(250, 500, 300, 300);
}
void drawBackground2(){
  //places grassland, triangle trees out of array and clouds as secondary background
  rectMode(CORNER);
  fill(34, 100, 34);
  noStroke();
  rect(0, y-150, x, 150);
  
  for(int j = 0; j < 50; j++){
    //use the values of the arrays
    fill(0, fill[j], 0);
    triangle(x1t[j], yt[j], x2t[j], yt[j], xht[j], ht[j]); 
  }
  
  stroke(200);
  fill(255,200);
  //cloud1
  rect(20,75,120,20);
  rect(50,60,80,30);
  rect(60,50,150,20);
  rect(70,55,100,25);
  rect(75,60,20,5);
  //cloud2
  rect(300,120,100,30);
  rect(265,110,90,25);
  //cloud3
  rect(450,50,120,20);
  rect(480,35,80,30);
  rect(490,25,150,20);
  rect(500,30,100,25);
  //cloud4
  rect(35,300,180,10);
  rect(55,295,240,10);
  //cloud5
  rect(285,270,120,7);
}

void drawForeground1(){
  //places triangle trees out of array as foreground
  for(int l = 0; l < 50; l++){
    noStroke();
    fill(0, fill2[l], fill3, alpha2[l]);
    triangle(x1t2[l], yt2[l], x2t2[l], yt2[l], xht2[l], ht2[l]); 
  }
}
void drawForeground2(){
  //day-night cycle
  fill(0,alpha);
  rect(0,0,x,y+50);
}

void change(){
  //changes alpha, fill3 per time
  if((alpha < 0) || (alpha > 255)){
    aloffset = -aloffset;
  }
  if((fill3 < 0) || (fill3 > 255)){
    foffset = -foffset;
  }
  alpha = alpha + aloffset;
  fill3 = fill3 + foffset;
 }
 
 void draw(){
   change();
   drawBackground1();
   drawBackground2();
   randy.display();
   randy.move();
   cartman.display();
   max.display();
   ol.handleCollisions(cartman,max,randy,keyType);
   cartman.moveLeft();
   max.move(1,1,0,800,0,600);
   
   drawForeground1();
   //drawForeground2();
 }
 
 
 void keyPressed(){
   keyType = key;
   println(keyType);
 
 }