class Randal {
    int avx;  //Move avatar from left to right
    int avy;
    float avscale;  //How much to scale avatar up or down
    IntList boundBox;
    boolean leftRight;
      /**
      *@param new_avatarX gives the initial X coordinate.
      *@param new_avatarY gives the initial Y coordinate.
      *@param new_scale says how much to scale the avatar drawing.
      **/
     IntList getBoundingBox(){
         setBoundingBox();
         //fill(255);
         //rectMode(CORNERS);
         //rect(boundBox.get(0),boundBox.get(1),boundBox.get(2),boundBox.get(3));
         //rectMode(CENTER);
         point(boundBox.get(0),boundBox.get(1));
         point(boundBox.get(2),boundBox.get(3));
         return boundBox;
       }
      void setBoundingBox(){
          boundBox.clear();
          boundBox.append(avx-25);
          boundBox.append(avy+74);
          boundBox.append(avx+25);
          boundBox.append(avy+225);
      }
    
    Randal(int new_avatarX, int new_avatarY, float new_scale) {
         avx= new_avatarX;
         avy= new_avatarY;
         avscale= new_scale;
         boundBox= new IntList();
      }
    /**
     *Move the Avatar according to its speed, direction, and current state.
    **/
    void move(){
      if (avx > 800){
        leftRight = false;
      }
      else if (avx < 0){
        leftRight = true;
      }
      if (leftRight){
        avx += 3;
      }
      else {
        avx -= 3;
      }
      //avx= avx-1;
      //if (avx<0){
      //    avx=width;
      //  }
    }
    
    /**
     *Display the Avatar object in relation to its current state.
    **/
    void display(){
        //Body
        stroke(200,0,50);
        fill(0,155,255);
        ellipse(avx+1*avscale,avy+155,10*avscale,135*avscale);
        
        // Head
        stroke(0,155,255);
        fill(200,0,50);
        rect(avx+1*avscale,avy+100,48*avscale,40*avscale);
        
        //mouth
        fill(0);
        rect(avx+1*avscale,avy+107,25*avscale,10*avscale);
        
        // Eyes
        fill(0);
        ellipse(avx+1*avscale,avy+93,13,15*avscale);
        fill(255);
        ellipse(avx+1*avscale,avy+93,5,17*avscale);
        
        // Legs
        stroke(0);
        line(avx-1*avscale,avy+230,240*avscale,205);
        line(avx+1*avscale,avy+205,275*avscale,230);
        
        // Arms
        line(avx+1*avscale,155,220*avscale,165);
        line(avx+1*avscale,165,255*avscale,177);
        setBoundingBox();
        getBoundingBox();
       }
 }