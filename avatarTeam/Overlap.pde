class Overlap{
  char keyType;

  void handleCollisions(Cartman cartman , AvatarMax max, Randal randy,char keyType){
    this.keyType = keyType;
    boolean maxCartmanOver = overlapget(cartman.boundingBox,max.bbMax);
    boolean maxRandalOver = overlapget(randy.boundBox,max.bbMax);
    boolean cartmanRandalOver = overlapget(cartman.boundingBox,randy.boundBox);
    if(maxCartmanOver){
      switch(keyType){
      case 'i':
        cartman.alpha = 255;
        break;
       case 'b':
         overlapActionCM(cartman , max);
         cartman.alpha = 255;
         break;
        case 't':
         cartman.alpha = 100;
          break;
        default:
          cartman.alpha = 255;
        }
    }
    
    if(maxRandalOver){
      switch(keyType){
      case 'i':
        break;
       case 'b':
         overlapActionMR(max , randy);
         break;
        case 't':
          //Change alpha
          break;
        }
    }
    
    if(cartmanRandalOver){
      switch(keyType){
      case 'i':
        break;
       case 'b':
         overlapActionCR(cartman , randy);
         break;
        case 't':
          //Change alpha
          break;
        }
    }
    
    

  }
  
  
  boolean overlapget(IntList bb1, IntList bb2) {
    // If bb1 is completely above, below,
    // lef or right of bb2, we have an easy reject.
      System.err.println("overlapget "+bb1.get(0) +","+bb1.get(1)+"     "+bb1.get(2) +","+bb1.get(3));
      System.err.println("overlapget "+bb2.get(0) +","+bb2.get(1)+"     "+bb2.get(2) +","+bb2.get(3));
      if ((bb1.get(0) > bb2.get(2) // bb1_left is right of bb2_right
          || bb1.get(1) > bb2.get(3)) // bb1_top is below bb2_bottom, now reverse them
          || (bb2.get(0) > bb1.get(2) // bb2_left is right of bb1_right
          || bb2.get(1) > bb1.get(3)) // bb2_top is below bb1_bottom, now reverse them
        ) {
         System.err.println("False");
        return false ;
      }
    // In this case one contains the other or they overlap.
    //println("Cartman: "+bb1);
    // println("Max: "+bb2);
    System.err.println("True");
    return true ;
  }
  
   void overlapActionCM(Cartman cartman  , AvatarMax max){
     cartman.flag = !cartman.flag;
     max.bx = !max.bx;
     max.by = !max.by;
   }
   
   void overlapActionMR(AvatarMax max,  Randal randy){
     randy.leftRight = !randy.leftRight;
     max.bx = !max.bx;
     max.by = !max.by;
   }
   
    void overlapActionCR(Cartman cartman  ,  Randal randy){
     randy.leftRight = !randy.leftRight;
     cartman.flag = !cartman.flag;
   }

}
